# Copyright (c) 2013-2017, The Linux Foundation. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above
#       copyright notice, this list of conditions and the following
#       disclaimer in the documentation and/or other materials provided
#       with the distribution.
#     * Neither the name of The Linux Foundation nor the names of its
#       contributors may be used to endorse or promote products derived
#       from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
# BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
# BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
# IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
#

on early-init
    mkdir /firmware 0771 system system
    mkdir /bt_firmware 0771 system system
    symlink /data/tombstones /tombstones
    mkdir /dsp 0771 media media

on init
    write /sys/module/qpnp_rtc/parameters/poweron_alarm 1

    # wait for bootdevice and symlink early
    wait /dev/block/platform/soc/1da4000.ufshc
    symlink /dev/block/platform/soc/1da4000.ufshc /dev/block/bootdevice
    restorecon_recursive /dev/block/bootdevice

    # start qseecomd early as we mount system/ early
    # vold needs keymaster that needs qseecomd
    start qseecomd

on fs
    mount_all fstab.qcom

    restorecon_recursive /persist
    mkdir /persist/data 0700 system system

on post-fs
    # Wait qseecomd started
    wait_for_prop sys.listeners.registered true

    write /dev/ipa 1

    insmod /vendor/lib/modules/exfat.ko

# OTG support
on property:persist.sys.oem.otg_support=true
    write /sys/class/power_supply/usb/otg_switch 1
on property:persist.sys.oem.otg_support=false
    write /sys/class/power_supply/usb/otg_switch 0

on post-fs-data
    mkdir /persist/wlan_bt 0700 system system
    mkdir /persist/audio 0755 system system
    mkdir /persist/subsys 0700 system system
    mkdir /data/vendor/hbtp 0750 system system
    mkdir /persist/qti_fp 0700 system system
    mkdir /data/misc/seemp 0700 system system
    mkdir /data/misc/fpc 0770 system system
    mkdir /data/misc/goodix 0770 system system
    mkdir /persist/fpc 0770 system system

    mkdir /data/tombstones 0771 system system
    mkdir /tombstones/modem 0771 system system
    mkdir /tombstones/lpass 0771 system system
    mkdir /tombstones/wcnss 0771 system system
    mkdir /tombstones/dsps 0771 system system
    mkdir /data/vendor/hbtp 0750 system system
    mkdir /data/misc/qvr 0770 system system
    mkdir /data/nfc 0770 nfc nfc
    mkdir /data/vendor/nfc 0770 nfc nfc
    mkdir /data/thermal 0771 system system
    mkdir /data/thermal/config 0771 system system
    chown system system /dev/sysmatdrv
    chown system system /dev/tiload_node
    chmod 0660 /dev/sysmatdrv
    chmod 644 /persist/audio/us_manual_cal
    chmod 660 /dev/tiload_node

#QDCM config file for sagit
    copy /system/etc/calib.cfg /data/vendor/display/calib.cfg
    copy /system/etc/qdcm_calib_data_jdi_fhd_cmd_incell_dsi_panel.xml /data/vendor/display/qdcm_calib_data_jdi_fhd_cmd_incell_dsi_panel.xml
    copy /system/etc/qdcm_calib_data_lgd_fhd_cmd_incell_dsi_panel.xml /data/vendor/display/qdcm_calib_data_lgd_fhd_cmd_incell_dsi_panel.xml
    copy /system/etc/jdi_fhd_cmd_incell_dsi_panel_ct.xml /data/vendor/display/jdi_fhd_cmd_incell_dsi_panel_ct.xml
    chmod 0644 /data/vendor/display/calib.cfg
    chmod 0644 /data/vendor/display/qdcm_calib_data_jdi_fhd_cmd_incell_dsi_panel.xml
    chmod 0644 /data/vendor/display/qdcm_calib_data_lgd_fhd_cmd_incell_dsi_panel.xml
    chmod 0644 /data/vendor/display/jdi_fhd_cmd_incell_dsi_panel_ct.xml
#QDCM config file for chiron
    copy /system/etc/jdi_fhd_video_dsi_panel_ct.xml /data/vendor/display/jdi_fhd_video_dsi_panel_ct.xml
    copy /system/etc/qdcm_calib_data_jdi_fhd_video_dsi_panel.xml /data/vendor/display/qdcm_calib_data_jdi_fhd_video_dsi_panel.xml
    chmod 0644 /data/vendor/display/jdi_fhd_video_dsi_panel_ct.xml
    chmod 0644 /data/vendor/display/qdcm_calib_data_jdi_fhd_video_dsi_panel.xml

on boot
    start rmt_storage
    start rfs_access

    ####Regionalization config and prop files####
    chmod 0644 /persist/speccfg/spec
    chmod 0644 /persist/speccfg/devicetype
    chmod 0644 /persist/speccfg/mbnversion
    chmod 0644 /persist/speccfg/.not_triggered
    chmod 0644 /persist/speccfg/vendor_ro.prop
    chmod 0644 /persist/speccfg/vendor_persist.prop
    chmod 0644 /persist/speccfg/submask
    chmod 0644 /persist/speccfg/partition
    chown system system /persist/speccfg/spec
    chown system system /persist/speccfg/devicetype
    chown system system /persist/speccfg/mbnversion
    chown system system /persist/speccfg/.not_triggered
    chown system system /persist/speccfg/vendor_ro.prop
    chown system system /persist/speccfg/vendor_persist.prop
    chown system system /persist/speccfg/submask
    chown system system /persist/speccfg/partition

    # access permission for Speaker SmartPA
    chmod 0666 /dev/i2c-10
    chmod 0777 /dev/elliptic0
    chmod 0777 /dev/elliptic1

    # NFC
    chmod 0666 /dev/nq-nci
    chown nfc nfc /dev/nq-nci

    # Fingerprint
    chown system system /dev/goodix_fp
    chown system system /sys/module/gf_spi/parameters/report_home_events
    chown system system /sys/bus/platform/devices/soc:fingerprint_fpc/irq
    chown system system /sys/bus/platform/devices/soc:fingerprint_fpc/irq_enable
    chown system system /sys/bus/platform/devices/soc:fingerprint_fpc/wakeup_enable
    chown system system /sys/bus/platform/devices/soc:fingerprint_fpc/hw_reset
    chown system system /sys/bus/platform/devices/soc:fingerprint_fpc/device_prepare
    chown system system /sys/bus/platform/devices/soc:fingerprint_fpc/fingerdown_wait
    chown system system /sys/bus/platform/devices/soc:fingerprint_fpc/vendor
    chown system system /sys/devices/soc/soc:fingerprint_fpc/enable_key_events
    chown system system /data/misc/fpc/calibration_image.pndat
    chmod 0660 /sys/devices/soc/soc:fingerprint_fpc/enable_key_events
    chmod 0700 /sys/bus/platform/devices/soc:fingerprint_fpc/irq
    chmod 0700 /sys/bus/platform/devices/soc:fingerprint_fpc/wakeup_enable
    chmod 0700 /sys/bus/platform/devices/soc:fingerprint_fpc/hw_reset
    chmod 0700 /sys/bus/platform/devices/soc:fingerprint_fpc/device_prepare
    chmod 0700 /sys/bus/platform/devices/soc:fingerprint_fpc/vendor
    chmod 0600 /data/misc/fpc/calibration_image.pndat
    chmod 0660 /sys/module/gf_spi/parameters/report_home_events

    # DT2W & Buttons
    chown system system /proc/touchpanel
    chown system system /sys/devices/soc/c179000.i2c/i2c-5/5-0020/input/input1/0dbutton
    chown system system /sys/devices/soc/c179000.i2c/i2c-5/5-0020/input/input1/wake_gesture
    chown system system /sys/devices/soc/c179000.i2c/i2c-5/5-0020/input/input1/reversed_keys_enable
    chmod 0660 /sys/devices/soc/c179000.i2c/i2c-5/5-0020/input/input1/0dbutton
    chmod 0660 /sys/devices/soc/c179000.i2c/i2c-5/5-0020/input/input1/wake_gesture
    chmod 0660 /sys/devices/soc/c179000.i2c/i2c-5/5-0020/input/input1/reversed_keys_enable
    chown system system /sys/class/leds/button-backlight1/brightness
    chown system system /proc/touchpanel/double_tap_enable
    chmod 0660 /proc/touchpanel/double_tap_enable

    # Vibrator permissions
    chown root system /sys/class/timed_output/vibrator/vtg_level
    chmod 0660 /sys/class/timed_output/vibrator/vtg_level

    # Wifi firmware reload path
    chown wifi wifi /sys/module/wlan/parameters/fwpath

on property:ro.boot.fingerprint=fpc
    setprop ro.hardware.fingerprint fpc
 
on property:ro.boot.fingerprint=goodix
    setprop ro.hardware.fingerprint goodix

on property:wc_transport.start_root=true
    start hci_filter_root

on property:wc_transport.start_root=false
    stop hci_filter_root

#Peripheral manager
service per_mgr /vendor/bin/pm-service
    class core
    user system
    group system
    ioprio rt 4
    shutdown critical

service per_proxy /vendor/bin/pm-proxy
    class core
    user system
    group system
    disabled

service readmac /vendor/bin/readmac
    class main
    user root
    group oem_2950 bluetooth
    capabilities CHOWN FOWNER
    oneshot
     
on property:init.svc.per_mgr=running
    start per_proxy

on property:sys.shutdown.requested=*
    stop per_proxy

service qseecomd /vendor/bin/qseecomd
    class core
    user root
    group root
    shutdown critical

service thermal-engine /system/vendor/bin/thermal-engine -T
    class main
    user root
    socket thermal-send-client stream 0666 system system
    socket thermal-recv-client stream 0660 system system
    socket thermal-recv-passive-client stream 0666 system system
    socket thermal-send-rule stream 0660 system system
    group root

service adsprpcd /system/vendor/bin/adsprpcd
    class main
    user media
    group media
    writepid /dev/cpuset/system-background/tasks

service imsqmidaemon /system/vendor/bin/imsqmidaemon
    class main
    user system
    socket ims_qmid stream 0660 system radio
    group radio log diag
    writepid /dev/cpuset/system-background/tasks

service imsdatadaemon /system/vendor/bin/imsdatadaemon
    class main
    user system
    socket ims_datad stream 0660 system radio
    group system wifi radio inet log diag
    disabled
    writepid /dev/cpuset/system-background/tasks
service pd_mapper /system/vendor/bin/pd-mapper
     class core

on property:vendor.ims.QMI_DAEMON_STATUS=1
    start imsdatadaemon

service ims_rtp_daemon /system/vendor/bin/ims_rtp_daemon
    class main
    user system
    group radio diag inet log
    disabled
    writepid /dev/cpuset/system-background/tasks

service vendor.imsrcsservice /system/vendor/bin/imsrcsd
    class main
    user system
    group radio diag inet log
    disabled
    writepid /dev/cpuset/system-background/tasks

on property:vendor.ims.DATA_DAEMON_STATUS=1
    start ims_rtp_daemon
    start vendor.imsrcsservice

service ppd /vendor/bin/mm-pp-dpps
    class late_start
    user system
    group system graphics
    socket pps stream 0660 system system
    disabled

on property:init.svc.hwcomposer-2-1=stopped
    stop ppd

on property:init.svc.hwcomposer-2-1=running
    start ppd

on property:init.svc.hwcomposer-2-1=restarting
    stop ppd

service energy-awareness /system/vendor/bin/energy-awareness
    class main
    user root
    group system
    oneshot
    writepid /dev/cpuset/system-background/tasks

service mmbn-daemon /system/bin/mmbn-daemon
    class main
    disabled
    oneshot

service hvdcp_opti /system/vendor/bin/hvdcp_opti
    class main
    user root
    group system wakelock
    writepid /dev/cpuset/system-background/tasks

service hci_filter_root /system/vendor/bin/wcnss_filter
    class late_start
    user bluetooth
    group bluetooth oem_2950 system
    disabled